require 'spec_helper'

describe 'dk_oracle_java::install' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }
      let(:packages) { %w( python-software-properties
                           software-properties-common
                           debconf-utils
                         ) }
      let(:deb_package) { 'oracle-java8-installer' }
      let(:deb_name) { 'shared/accepted-oracle-license-v1-1' }
      let(:deb_types) { %w(select seen) }  # select is type, seen is flag

      it 'should install prerequisite packages' do
        packages.each do |package|
          is_expected.to contain_package(package).with(ensure: 'present')
        end
      end

      it 'should install ppa:webupd8team/java' do
        is_expected.to contain_apt__ppa ('ppa:webupd8team/java')
      end

      it 'should set oracle license type select and flag seen' do
        deb_types.each do |deb_type|
          command = "echo '#{deb_package} #{deb_name} #{deb_type} true' | debconf-set-selections"
          is_expected.to contain_exec(command)
        end
      end

      it 'should install package nginx' do
        is_expected.to contain_package(deb_package).with(ensure: "8u171-1~webupd8~0")
      end

      it { is_expected.to compile }
    end
  end
end
