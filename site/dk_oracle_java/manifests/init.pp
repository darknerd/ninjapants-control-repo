# dk_oracle_java
#
# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include dk_oracle_java
class dk_oracle_java (
  $docroot     = $::dk_oracle_java::params::docroot,
  Optional[String] $local_repo  = $::dk_oracle_java::params::local_repo,
  $version     = $::dk_oracle_java::params::version,
  $javas       = $::dk_oracle_java::params::javas,
) inherits ::dk_oracle_java::params {
    include ::dk_oracle_java::install

    #Class['::dk_oracle_java::install']
}
