# dk_oracle_java::params
#
# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include dk_oracle_java::params
class dk_oracle_java::params {
  $docroot = '/var/www/java'
  $local_repo = undef
  $version = '8u171'
  $javas = {
    '8u171' => {
        'build' => 'b11',
        'hashcode' => '512cd62ec5174c3487ac17c61aaa89e8'
      },
  }
}
