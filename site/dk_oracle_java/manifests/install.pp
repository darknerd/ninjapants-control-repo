# dk_oracle_java::install
#
# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include dk_oracle_java::install
class dk_oracle_java::install inherits dk_oracle_java {
  # Global Resource Settings
  Class['apt::update'] -> Package <| provider == 'apt' |>
  Exec { path => [ '/bin', '/usr/bin', '/sbin' ] }

  # Local Constants
  $packages = [
    'python-software-properties',
    'software-properties-common',
    'debconf-utils'
  ]

  $deb_package = 'oracle-java8-installer'
  $deb_name = 'shared/accepted-oracle-license-v1-1'
  $deb_types = ['select','seen']

  package { $packages:
    ensure => present,
  }

  include apt
  apt::ppa { 'ppa:webupd8team/java': }

  $deb_types.each |$deb_type| {
    exec {"echo '$deb_package $deb_name $deb_type true' | debconf-set-selections": }
  }

  #$version = '8u171'

  package { $deb_package:
    ensure => "$version-1~webupd8~0",
  }

}
