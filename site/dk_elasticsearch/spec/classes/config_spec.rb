require 'spec_helper'

describe 'dk_elasticsearch::config' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }
      let(:node_params) do
        {
          'jvm_heap_size' => '512m',
          'cluster_name'  => 'dk_cluster',
          'network_host'  => '1.2.3.4',
          'unicast_hosts' => %w(1.2.3.4 1.2.3.5 1.2.3.6)
        }
      end

      let(:limits_entry) do
        user, type, item, value = 'elasticsearch', '-', 'nofile', '65536'
        "%-15s %-5s %8s %14s" % [user, type, item, value]
      end

      it 'should enable limits.conf for processes started by init.d on trusty' do
        command = "sed -r -i 's/#?\\s+(session\\s+required\\s+pam_limits.so)/\\1/' /etc/pam.d/su"
        is_expected.to contain_exec(command)
      end

      it 'should configure ulimit for elasticsearch user' do
        command = "sed -i '/# End of file/ i\\#{limits_entry}' /etc/security/limits.conf"
        is_expected.to contain_exec(command)
      end

      it 'should configure /etc/elasticsearch/elasticsearch.yml' do
        is_expected.to contain_file('/etc/elasticsearch/elasticsearch.yml').with(
          owner: 'root',
          group: 'elasticsearch',
          mode:  '0660'
        )
      end

      it 'should configure /etc/elasticsearch/jvm.options' do
        is_expected.to contain_file('/etc/elasticsearch/jvm.options').with(
          owner: 'root',
          group: 'elasticsearch',
          mode:  '0660'
        )
      end

      it 'should configure /etc/elasticsearch/log4j2.properties' do
        is_expected.to contain_file('/etc/elasticsearch/log4j2.properties').with(
          owner: 'root',
          group: 'elasticsearch',
          mode:  '0660'
        )
      end

      it { is_expected.to compile }
    end
  end
end
