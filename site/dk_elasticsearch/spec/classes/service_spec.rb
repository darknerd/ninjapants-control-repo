require 'spec_helper'

describe 'dk_elasticsearch::service' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it 'should start and enable elasticsearch service' do
        is_expected.to contain_service('elasticsearch').with(
          :ensure => 'running',
          :enable => true,
        )
      end

      it { is_expected.to compile }
    end
  end
end
