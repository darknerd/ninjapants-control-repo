require 'spec_helper'

describe 'dk_elasticsearch::install' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it 'should install package apt-transport-https' do
        is_expected.to contain_package('apt-transport-https').with(ensure: 'present')
      end

      it 'should add apt source elastic-6.x' do
        is_expected.to contain_apt__source('elastic-6.x').with(
          release: 'main',
          repos: 'stable',
          location: 'https://artifacts.elastic.co/packages/6.x/apt',
          key: {
            'id' => '46095ACC8548582C1A2699A9D27D666CD88E42B4',
            'source' => 'https://artifacts.elastic.co/GPG-KEY-elasticsearch'
          }
        )
      end

      it 'should install package elasticsearch' do
        is_expected.to contain_package('elasticsearch').with(ensure: 'present')
      end

      it { is_expected.to compile }
    end
  end
end
