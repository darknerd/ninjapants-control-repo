# dk_elasticsearch::params
#
# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include dk_elasticsearch::params
class dk_elasticsearch::params {
  $jvm_heap_size = '512m'
  $cluster_name  = 'dk_cluster'
  $network_host  = $::ipaddress
  $unicast_hosts = [] 
}
