# dk_elasticsearch::config
#
# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include dk_elasticsearch::config
class dk_elasticsearch::config inherits dk_elasticsearch {
  Exec { path => [ '/bin', '/usr/bin', '/sbin' ] }

  $sed_cmd = 's/#?\s+(session\s+required\s+pam_limits.so)/\1/'
  exec {"sed -r -i '$sed_cmd' /etc/pam.d/su": }

  # adjust limit entry for elasticsearch
  $user = 'elasticsearch'
  $type = '-'
  $item = 'nofile'
  $value = '65536'
  $limits_entry = "$user   $type       $item          $value"

  exec {"sed -i '/# End of file/ i\\$limits_entry' /etc/security/limits.conf": }

  file { '/etc/elasticsearch/elasticsearch.yml':
    owner   => 'root',
    group   => 'elasticsearch',
    mode    => '0660',
    content => template("dk_elasticsearch/elasticsearch.yml.erb")
  }

  file { '/etc/elasticsearch/jvm.options':
    owner   => 'root',
    group   => 'elasticsearch',
    mode    => '0660',
    content => template("dk_elasticsearch/jvm.options.erb"),
  }

  file { '/etc/elasticsearch/log4j2.properties':
    owner   => 'root',
    group   => 'elasticsearch',
    mode    => '0660',
    content => template("dk_elasticsearch/log4j2.properties.erb")
  }

}
