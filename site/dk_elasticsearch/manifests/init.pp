# dk_elasticsearch
#
# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include dk_elasticsearch
class dk_elasticsearch (
  $jvm_heap_size = $::dk_elasticsearch::params::jvm_heap_size,
  $cluster_name  = $::dk_elasticsearch::params::cluster_name,
  $network_host  = $::dk_elasticsearch::params::network_host,
  $unicast_hosts = $::dk_elasticsearch::params::unicast_hosts,
) inherits ::dk_elasticsearch::params {
    contain ::dk_elasticsearch::install
    contain ::dk_elasticsearch::config
    contain ::dk_elasticsearch::service

    Class['::dk_elasticsearch::install']
    -> Class['::dk_elasticsearch::config']
    ~> Class['::dk_elasticsearch::service']
}
