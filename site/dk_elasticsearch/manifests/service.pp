# dk_elasticsearch::service
#
# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include dk_elasticsearch::service
class dk_elasticsearch::service inherits dk_elasticsearch {
  service { 'elasticsearch':
    ensure => running,
    enable => true,
  }
}
