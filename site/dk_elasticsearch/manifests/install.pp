# dk_elasticsearch::install
#
# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include dk_elasticsearch::install
class dk_elasticsearch::install inherits dk_elasticsearch {
  Class['apt::update'] -> Package <| provider == 'apt' |>

  package { 'apt-transport-https':
    ensure => present,
  }

  apt::source { 'elastic-6.x':
    location => 'https://artifacts.elastic.co/packages/6.x/apt',
    release  => 'main',      # distribution
    repos    => 'stable',  # components
    key      => {
      'id' => '46095ACC8548582C1A2699A9D27D666CD88E42B4',
      'source' => 'https://artifacts.elastic.co/GPG-KEY-elasticsearch',
    },
  }

  package { 'elasticsearch':
    ensure => present,
  }

}
