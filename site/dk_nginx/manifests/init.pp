# dk_nginx
#
# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include dk_nginx
class dk_nginx {
  contain ::dk_nginx::install
  contain ::dk_nginx::config
  contain ::dk_nginx::service

  Class['::dk_nginx::install']
  -> Class['::dk_nginx::config']
  ~> Class['::dk_nginx::service']
}
