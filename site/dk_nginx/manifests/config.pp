# dk_nginx::config
#
# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include dk_nginx::config
class dk_nginx::config {
  file { ['/etc/nginx/sites-available','/etc/nginx/sites-enabled']:
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }

  file { "/etc/nginx/config.d/default.conf":
    ensure => absent,
  }

  file { "/etc/nginx/nginx.conf":
    ensure => file,
    source => "puppet:///modules/dk_nginx/nginx.conf",
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
  }

  file { "/var/www":
    ensure => directory,
    owner  => 'www-data',
    group  => 'www-data',
    mode   => '0755'
  }

}
