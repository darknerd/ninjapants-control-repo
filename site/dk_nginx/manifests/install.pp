# dk_nginx::install
#
# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include dk_nginx::install
class dk_nginx::install {

  apt::source { 'nginx':
    location => 'http://nginx.org/packages/ubuntu/',
    release  => $lsbdistcodename, # distribution
    repos    => 'nginx',          # components
    key      => {
      'id' => '573BFD6B3D8FBC641079A6ABABF5BD827BD9BF62',
      'source' => 'http://nginx.org/keys/nginx_signing.key',
    },
  }

  package { 'nginx':
    ensure => present,
    require => Class['apt::update']
  }

}
