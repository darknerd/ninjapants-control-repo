require 'spec_helper'

describe 'dk_nginx::config' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }
      let(:sites_dirs) { %w(sites-available sites-enabled) }

      it "should create '/etc/nginx/nginx.conf'" do
        is_expected.to contain_file('/etc/nginx/nginx.conf').with(
          ensure: 'file',
          owner:  'root',
          group:  'root',
          mode:   '0644'
        )
      end

      it 'should create nginx site directories' do
        sites_dirs.each do |dir|
          is_expected.to contain_file("/etc/nginx/#{dir}").with(
            ensure: 'directory',
            owner: 'root',
            group: 'root',
            mode: '0755'
          )
        end
      end

      it "should delete '/etc/nginx/config.d/default.conf'" do
        is_expected.to contain_file('/etc/nginx/config.d/default.conf').with(
          ensure: 'absent'
        )
      end

      it "should create '/var/www'" do
        is_expected.to contain_file('/var/www').with(
          ensure: 'directory',
          owner:  'www-data',
          group:  'www-data',
          mode:   '0755'
        )
      end

      it { is_expected.to compile }
    end
  end
end
