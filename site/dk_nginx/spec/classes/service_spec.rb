require 'spec_helper'

describe 'dk_nginx::service' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it 'should start and enable nginx service' do
        is_expected.to contain_service('nginx').with(
          :ensure => 'running',
          :enable => true,
        )
      end

      it { is_expected.to compile }
    end
  end
end
