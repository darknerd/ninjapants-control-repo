require 'spec_helper'

describe 'dk_nginx::install' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it 'should add apt source nginx' do
        is_expected.to contain_apt__source('nginx').with(
          release: facts[:lsbdistcodename],
          repos: 'nginx',
          location: 'http://nginx.org/packages/ubuntu/',
          key: {
            'id' => '573BFD6B3D8FBC641079A6ABABF5BD827BD9BF62',
            'source' => 'http://nginx.org/keys/nginx_signing.key'
          }
        )
      end

      it 'should install package nginx' do
        is_expected.to contain_package('nginx').with(ensure: 'present')
      end

      it { is_expected.to compile }
    end
  end
end
