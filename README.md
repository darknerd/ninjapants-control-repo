# **Puppet Control Repository**

This is a puppet control repository for managing servers using Puppet 5.

- Joaquin Menchaca (2016年5月5日)

## **Small Test Environment**

There's a `Vagrantfile` for testing these modules out manually that I use for R&D process.  To use this with `puppet apply`, you'll need to go through vendoring modules process on the virtual guest instance after running `vagrant up && vagrant ssh`.

## **Running Unit Tests**

```bash
cd site/$module_name/
rake spec
```

## **Vendoring Modules Process**

On target system that needs to run this using `puppet apply`, you vendor modules into a local modules directory.  

You can use Puppet's embedded ruby by putting `/opt/puppetlabs/puppet/bin/ruby` into your path, or install a matching ruby version of your own and add this to the path:

### **Prerequisite: R10K**

```bash
PUPPET_RUBY_VERSION=$(
  /opt/puppetlabs/puppet/bin/ruby --version | \
  awk '{print $2}' | \
  cut -dp -f1
)
gpg --keyserver hkp://keys.gnupg.net \
    --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 \
                7D2BAF1CF37B13E2069D6956105BD0E739499BDB

curl -sSL https://get.rvm.io | bash
source /etc/profile.d/rvm.sh
rvm install ${PUPPET_RUBY_VERSION}
gem install r10k
```

### **Download and Install Modules**

```bash
r10k puppetfile install
```

## **Running Puppet Convergence**

Assuming your current guest system is configured in `manifests/site.pp`, run

```bash
cd /vagrant # if using vagrant environment
puppet apply manifests/site.pp --modulepath=site:modules
```
